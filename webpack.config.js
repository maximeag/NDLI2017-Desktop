module.exports = {
    entry: {
        app: "./src/main/App.ts"
    },
    output: {
        filename: "[name].js",
        path: __dirname + "/dist"
    },

    devtool: "source-map",

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            {test: /\.tsx?$/, loader: "awesome-typescript-loader"},

            {enforce: "pre", test: /\.js$/, loader: "source-map-loader"}
        ]
    },

    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
};