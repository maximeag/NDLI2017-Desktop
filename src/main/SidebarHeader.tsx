import * as React from "react";
import {Badge} from "./Badge";

interface SidebarHeaderProps {
    status: string;
}

export class SidebarHeader extends React.Component<SidebarHeaderProps, {}> {

    render(): React.ReactNode {
        return (
            <div style={{padding: 16, borderBottom: '1px solid #e0e0e0', display: 'flex', justifyContent: 'space-between', alignContent: 'center'}}>
                <div style={{fontSize: 24, fontWeight: 700, color: '#424242'}}>ACCIDENT</div>
                <Badge text={this.props.status}/>
            </div>
        )
    }
}