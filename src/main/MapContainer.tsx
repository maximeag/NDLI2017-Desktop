import * as React from "react";
import GoogleMapReact from "google-map-react";

interface MapContainerProps {
    longitude: number;
    latitude: number;
}

export class MapContainer extends React.Component<MapContainerProps, {}> {

    render(): React.ReactNode {

        return (
            <div style={{position: 'absolute', top: 0, right: 400, bottom: 0, left: 0}}>
                <GoogleMapReact
                    center={{lat: this.props.latitude, lng: this.props.longitude}}
                    defaultCenter={{lat: 0, lng: 0}}
                    defaultZoom={11}>
                </GoogleMapReact>
            </div>
        )
    }
}