import * as React from "react";
import {SidebarHeader} from "./SidebarHeader";
import {SidebarContact} from "./SidebarContact";
import {SidebarIndications} from "./SidebarIndications";

interface SidebarProps {

    phone: string;
}

export class Sidebar extends React.Component<SidebarProps, {}> {

    render(): React.ReactNode {

        return (
            <div style={{position: 'absolute', top: 0, right: 0, bottom: 0, width: 400}}>
                <SidebarHeader status={this.props.phone ? 'EN COURS' : 'EN ATTENTE'}/>
                <SidebarContact phone={this.props.phone}/>
                <SidebarIndications/>
            </div>
        )
    }
}