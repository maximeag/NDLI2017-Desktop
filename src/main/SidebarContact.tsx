import * as React from "react";

interface SidebarContactProps {

    phone: string;
}

export class SidebarContact extends React.Component<SidebarContactProps, {}> {

    render(): React.ReactNode {
        return (
            <div style={{padding: 16, borderBottom: '1px solid #e0e0e0'}}>
                <div style={{fontSize: 20, color: '#616161'}}>CONTACT</div>
                <div style={{fontSize: 50, textAlign: 'center', fontWeight: 700, color: '#212121'}}>{this.props.phone}</div>
            </div>
        );
    }
}