import * as React from "react";
import {Question} from "./Question";

const answers = [
    {
        question: "Êtes-vous blessé ?",
        answer: false
    }, {
        question: "Le lieu est-il sécurisé ?",
        answer: false
    }, {
        question: "La/Les victime(s) est/sont elle(s) consciente(s) ?",
        answer: true
    }, {
        question: "Respire(nt)-elle(s) normalement ?",
        answer: true
    }
];

export class SidebarIndications extends React.Component<{}, {}> {


    render(): React.ReactNode {

        let questions = answers.map(a => <Question question={a.question} answer={a.answer} />);

        return (
            <div style={{padding: 16}}>
                <div style={{fontSize: 20, color: '#616161'}}>INDICATIONS</div>
                <div style={{padding: 8}}>
                    {questions}
                </div>
            </div>
        );
    }
}