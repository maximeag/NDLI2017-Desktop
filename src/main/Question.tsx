import * as React from "react";

interface QuestionProps {
    question: string;
    answer: boolean;
}

export class Question extends React.Component<QuestionProps, {}> {

    render(): React.ReactNode {
        return (
            <div style={{padding: 8, fontSize: 18}}>
                <div>{'- ' + this.props.question}</div>
                <div style={{fontWeight: 'bold'}}>{'> ' + (this.props.answer ? 'Oui' : 'Non')}</div>
            </div>
        );
    }
}