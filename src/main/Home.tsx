import * as React from "react";
import {MapContainer} from "./MapContainer";
import {Sidebar} from "./Sidebar";
import * as SocketIOClient from "socket.io-client";

interface HomeState {

    latitude: number;
    longitude: number;
    phone: string;
}

export class Home extends React.Component<null, HomeState> {

    private socket: SocketIOClient.Socket;

    constructor(props) {
        super(props);

        this.state = {
            latitude: 50.6096977,
            longitude: 3.1368249,
            phone: "06 43 81 62 18"
        }
    }


    render(): React.ReactNode {

        return (
            <div>
                <MapContainer latitude={this.state.latitude} longitude={this.state.longitude}/>
                <Sidebar phone={this.state.phone}/>
            </div>
        );
    }
}