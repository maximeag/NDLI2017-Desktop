import * as React from "react";

interface BadgeProps {

    text: string;
}

export class Badge extends React.Component<BadgeProps, {}> {

    render(): React.ReactNode {
        return (
            <div style={{padding: 3, backgroundColor: '#ff9800', color: 'white', borderRadius: 2}}>
                {this.props.text}
            </div>
        )
    }
}