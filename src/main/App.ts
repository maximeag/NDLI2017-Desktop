import {Home} from "./Home";
import * as React from "react";
import * as ReactDOM from "react-dom";

class App {

    public static main() {

        const home = React.createElement(Home);
        const root = document.getElementById('root');

        ReactDOM.render(home, root);
    }
}

App.main();